# frozen_string_literal: true

Rails.application.routes.draw do
  resources :products, only: [] do
    resources :brands, module: :products, only: [:index]
  end
  resources :line_items
  resources :carts
  resources :instruments
  devise_for :users, controllers: {
    registrations: 'registrations'
  }
  root 'instruments#index'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

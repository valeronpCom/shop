# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Instrument, type: :model do
  it 'validate presence of title' do
    expect(Instrument.new(title: '123')).to_not be_valid
  end

  it 'validate presence of model' do
    expect(Instrument.new(model: '123')).to_not be_valid
  end

  it 'validate presence of price' do
    expect(Instrument.new(price: '123')).to_not be_valid
  end

  it 'validate length of description' do
    description = 'q' * 1000
    i = Instrument.new(description: description)
    expect(i).to_not be_valid
  end

  describe 'validate destroy' do
    let(:line_item) { LineItem.new }
    let(:instrument) { Instrument.new(line_items: [line_item]) }
    it "doesn't allow to delete" do
      instrument.destroy
      expect(instrument.errors.full_messages).to eq(['Line items present'])
    end
  end
end

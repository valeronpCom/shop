# frozen_string_literal: true

require 'rails_helper'

describe InstrumentsController do
  let(:user) { User.create!(id: 1, name: 'First User', email: 'test_email@some.com', password: 12_345_678, created_at: DateTime.now, updated_at: DateTime.now) }
  let(:product_first) { Product.create!(id: 1, product_name: 'Product Name', created_at: DateTime.now, updated_at: DateTime.now) }
  let(:brand_first) { Brand.create!(id: 1, brand_name: 'First Brand', created_at: DateTime.now, updated_at: DateTime.now) }
  let(:instrument) do
    Instrument.create!(id: 1, model: 'First Model', description: '3ffefw', title: 'title', price: 300,
                       created_at: DateTime.now, updated_at: DateTime.now, user_id: 1, brand_id: 1, product_id: 1)
  end

  let(:message) { "The user doesn't have permission to do it." }

  describe '#destroy' do
    before do
      expect(subject).to receive(:authenticate_user!).and_return(true)
      expect(subject).to receive(:current_user).and_return(user)

      [product_first, brand_first, instrument]
    end

    it 'returns message about not deleting instrument' do
      delete :destroy, params: { id: instrument.id }

      expect(response).to redirect_to(root_path)
      expect(flash[:notice]).to match(message)
    end
  end
end

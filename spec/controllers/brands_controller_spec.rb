# frozen_string_literal: true

require 'rails_helper'

describe Products::BrandsController do
  let(:result) do
    {
      'brands' => [
        [1, 'First Brand'],
        [2, 'Second Brand']
      ]
    }
  end
  let(:product) { Product.create!(id: 1, product_name: 'Product Name', created_at: DateTime.now, updated_at: DateTime.now) }
  let(:product_second) { Product.create!(id: 2, product_name: 'Second Product', created_at: DateTime.now, updated_at: DateTime.now) }
  let(:brand_first) { Brand.create!(id: 1, brand_name: 'First Brand', created_at: DateTime.now, updated_at: DateTime.now) }
  let(:brand_second) { Brand.create!(id: 2, brand_name: 'Second Brand', created_at: DateTime.now, updated_at: DateTime.now) }
  let(:brand_third) { Brand.create!(id: 3, brand_name: 'Third Brand', created_at: DateTime.now, updated_at: DateTime.now) }
  let(:brand_product_first) { BrandProduct.create!(product: product, brand: brand_first, created_at: DateTime.now, updated_at: DateTime.now) }
  let(:brand_product_second) { BrandProduct.create!(product: product, brand: brand_second, created_at: DateTime.now, updated_at: DateTime.now) }
  let(:brand_product_third) { BrandProduct.create!(product: product_second, brand: brand_third, created_at: DateTime.now, updated_at: DateTime.now) }

  describe 'GET #index' do
    before do
      [product, product_second, brand_first, brand_second, brand_third, brand_product_first, brand_product_second, brand_product_third]
    end

    it 'returns result in JSON format' do
      get :index, params: { product_id: product.id }

      expect(response).to have_http_status(:ok)
      expect(response.body).to eq(JSON(result))
    end
  end
end

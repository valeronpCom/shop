# frozen_string_literal: true

require 'rails_helper'

describe InstrumentCreator do
  let(:user) do
    user = User.first_or_create!(name: 'First User',
                                 email: 'emailWhichWasNotTaken@some.com',
                                 password: 12_345_678)
    user.add_role :admin
  end
  let(:product) { Product.create!(product_name: 'Product Name') }
  let(:brand) { Brand.create!(brand_name: 'First Brand') }
  let(:number) { Instrument.count }
  let(:mailer) { instance_double(InstrumentsMailer) }
  let(:message_delivery) { instance_double(ActionMailer::MessageDelivery) }
  let(:params) do
    {
      brand_id: brand.id,
      product_id: product.id,
      title: 'title',
      description: '3ffefw',
      price: 300,
      model: 'model'
    }
  end
  let(:invalid_params) do
    {
      title: 'title',
      description: '3ffefw',
      price: 300
    }
  end

  context 'when params are valid' do
    subject { described_class.new(user, params) }

    before do
      allow(InstrumentsMailer).to receive(:with).and_return(mailer)
      allow(mailer).to receive(:instrument_created).and_return(message_delivery)
      allow(message_delivery).to receive(:deliver_later)
    end

    it 'returns new instrument' do
      expect(subject.call).to eq(Instrument.last)
    end

    it 'increments the number of instruments' do
      expect { subject.call }.to change { Instrument.count }.from(number).to(number + 1)
    end

    it 'sends messages to admins' do
      subject.call
      expect(message_delivery).to have_received(:deliver_later)
    end
  end

  context 'when params are invalid' do
    subject { described_class.new(user, invalid_params) }

    it 'does not create new instrument' do
      expect { subject.call }.to change { Instrument.count }.by(0)
    end

    it 'returns invalid instrument without creation' do
      expect(subject.call.new_record?).to eq(true)
    end
  end
end

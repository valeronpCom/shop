# frozen_string_literal: true

require 'rails_helper'

describe InstrumentInfoSender do
  let(:user) do
    user = User.first_or_create!(name: 'First User',
                                 email: 'emailWhichWasNotTaken@some.com',
                                 password: 12_345_678)
    user.add_role :admin
  end
  let(:product) { Product.create!(product_name: 'Product Name') }
  let(:brand) { Brand.create!(brand_name: 'First Brand') }
  let(:instrument) do
    Instrument.create!(brand_id: brand.id,
                       product_id: product.id,
                       title: 'title',
                       description: '3ffefw',
                       price: 300,
                       model: 'model')
  end
  let(:mailer) { instance_double(InstrumentsInfoMailer) }
  let(:brand_instruments) { instance_double(BrandInstrumentsPresenter)}
  let(:message_delivery) { instance_double(ActionMailer::MessageDelivery) }
  let(:relation) { instance_double(ActiveRecord::Relation) }
  let(:result) { { 'First Brand' => 1 } }

  describe '#send_notification' do
    context 'when params are valid' do
      before do
        user

        allow(BrandInstrumentsPresenter).to receive(:new).and_return(brand_instruments)
        allow(brand_instruments).to receive(:call).and_return(result)
        allow(InstrumentsInfoMailer).to receive(:with).and_return(mailer)
        allow(mailer).to receive(:information_sended).and_return(message_delivery)
        allow(message_delivery).to receive(:deliver_later)
      end

      it 'checks getting the result' do
        subject.send_notification

        expect(InstrumentsInfoMailer).to have_received(:with).with(user: User.last, result: result)
        expect(mailer).to have_received(:information_sended)
        expect(message_delivery).to have_received(:deliver_later)
      end
    end
  end
end

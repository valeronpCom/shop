# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.where(email: 'test_email@some.com').first_or_create!(email: 'test_email@some.com',
                                                                 name: 'Test Name',
                                                                 password: 12_345_678)

user.add_role :admin

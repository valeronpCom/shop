# frozen_string_literal: true

class ChangeInstrumentsTable < ActiveRecord::Migration[6.1]
  def change
    remove_column :instruments, :brand, :string
    remove_column :instruments, :instrument_type, :string
    add_reference :instruments, :brand, null: false, foreign_key: true
    add_reference :instruments, :product, null: false, foreign_key: true
  end
end

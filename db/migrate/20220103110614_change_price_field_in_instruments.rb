# frozen_string_literal: true

class ChangePriceFieldInInstruments < ActiveRecord::Migration[6.1]
  def change
    change_column :instruments, :price, :decimal, precision: 15, scale: 10, default: 0
  end
end

# frozen_string_literal: true

class Product < ApplicationRecord
  has_many :brand_products
  has_many :brands, through: :brand_products
  has_many :instruments
end

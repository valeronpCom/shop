# frozen_string_literal: true

class Brand < ApplicationRecord
  has_many :brand_products
  has_many :products, through: :brand_products
  has_many :instruments
end

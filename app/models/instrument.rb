# frozen_string_literal: true

class Instrument < ApplicationRecord
  before_destroy :not_referenced_by_any_line_item
  belongs_to :user, optional: true
  belongs_to :brand
  belongs_to :product
  has_many :line_items

  mount_uploader :image, ImageUploader
  serialize :image, JSON

  validates :title, :price, :model, presence: true
  validates :description, length: { maximum: 1000, too_long: '%{count} characters is the maximum allowed. ' }
  validates :title, length: { maximum: 150, too_long: '%{count} characters is the maximum allowed. ' }
  validates :price, numericality: { only_integer: true }, length: { maximum: 7 }

  BRAND = %w[AKG Audio-Technica Behringer Manley Neumann Oktava Shure].freeze
  TYPE = %w[Guitar Drums Keys Microphones Cases Cables Accessories].freeze

  private

  def not_referenced_by_any_line_item
    return if line_items.empty?

    errors.add(:base, 'Line items present')
    throw :abort
  end
end

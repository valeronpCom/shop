# frozen_string_literal: true

class InstrumentsInfoMailer < ApplicationMailer
  def information_sended
    @user = params[:user]
    @result = params[:result]
    mail to: @user.email,
         subject: 'Information about brands has been sended'
  end
end

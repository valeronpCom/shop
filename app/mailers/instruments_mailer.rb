# frozen_string_literal: true

class InstrumentsMailer < ApplicationMailer
  def instrument_created
    @user = params[:user]
    @instrument = params[:instrument]
    mail to: @user.email,
         subject: 'Instrument created'
  end
end

document.addEventListener("turbolinks:load", function() {
    jQuery(function() {
        return $('#subcategory_select').change(function() {
            const brandSelect = $('#brand_select')[0];
            const productId = $('#instrument_product_id')[0].value;
            $.ajax({
                type:"GET",
                url:"/products/" + productId + "/brands",
                dataType:"json",
                success: function(response) {
                    const ajaxVariable = response.brands;
                    const oldSelector = $('#product_select')[0];
                    if (oldSelector) {
                        oldSelector.remove();
                    }
                    const selectList = document.createElement("select");
                    selectList.setAttribute("id", "product_select");
                    selectList.setAttribute("name", "instrument[brand_id]");
                    brandSelect.appendChild(selectList);
                    $.each(ajaxVariable, function(index, value) {
                        $('#product_select')
                            .append($("<option></option>")
                                .attr("value", value[0])
                                .text(value[1]));
                    });
                }
            });
        });
    });
});
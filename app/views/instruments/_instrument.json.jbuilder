# frozen_string_literal: true

json.extract! instrument, :id, :brand, :model, :description, :type, :title, :price, :created_at, :updated_at
json.url instrument_url(instrument, format: :json)

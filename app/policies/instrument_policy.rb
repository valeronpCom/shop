# frozen_string_literal: true

class InstrumentPolicy < ApplicationPolicy
  attr_reader :user

  def initialize(user)
    super(user, nil)
  end

  def access?
    current_user.has_role? :admin
  end

  def update?
    current_user.has_role? :admin
  end

  def edit?
    current_user.has_role? :admin
  end
end

# frozen_string_literal: true

class InstrumentInfoSender
  def send_notification
    User.with_role(:admin).each do |f|
      InstrumentsInfoMailer.with(user: f, result: BrandInstrumentsPresenter.new.call).information_sended.deliver_later(wait: 5.seconds)
    end
  end

end

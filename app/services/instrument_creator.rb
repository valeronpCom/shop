# frozen_string_literal: true

class InstrumentCreator
  def initialize(current_user, instrument_params)
    @current_user = current_user
    @instrument_params = instrument_params
  end

  def call
    instrument = Instrument.create(@instrument_params.merge(user_id: @current_user.id))
    send_notification(instrument) unless instrument.new_record?
    instrument
  end

  private

  def send_notification(instrument)
    User.with_role(:admin).each do |f|
      InstrumentsMailer.with(user: f, instrument: instrument).instrument_created.deliver_later(wait: 5.seconds)
    end
  end
end

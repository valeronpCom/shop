# frozen_string_literal: true

class BrandInstrumentsPresenter
  def call
    brand_instruments
  end

  private

  def brand_instruments
    @_brand_instruments ||= Brand.joins(:instruments).group(:brand_name).count
  end
end

# frozen_string_literal: true

class EmailSenderWorker
  include Sidekiq::Worker

  def perform
    InstrumentInfoSender.new.send_notification
  end

  Sidekiq::Cron::Job.create(name: 'Email sender worker - every day at 18.00', cron: '0 18 * * *', class: 'EmailSenderWorker')
end

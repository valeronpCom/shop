# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit
  include CurrentCart
  before_action :set_cart
end

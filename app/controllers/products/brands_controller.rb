# frozen_string_literal: true

module Products
  class BrandsController < ApplicationController
    def index
      @brands = Brand.joins(brand_products: :product).where(products: { id: params[:product_id] }).pluck(:id, :brand_name)
      render json: { brands: @brands }
    end
  end
end

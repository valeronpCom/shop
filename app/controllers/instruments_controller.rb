# frozen_string_literal: true

class InstrumentsController < ApplicationController
  before_action :set_instrument, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: %i[index show]
  before_action :check_instrument_policy, only: %i[new edit create update destroy]

  # GET /instruments or /instruments.json
  def index
    @instruments = Instrument.all.order('created_at desc')
  end

  # GET /instruments/1 or /instruments/1.json
  def show; end

  # GET /instruments/new
  def new
    @instrument = current_user.instruments.build
  end

  # GET /instruments/1/edit
  def edit; end

  # POST /instruments or /instruments.json
  def create
    @instrument = InstrumentCreator.new(current_user, instrument_params).call
    if !@instrument.new_record?
      redirect_to @instrument, notice: 'Instrument was successfully created.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /instruments/1 or /instruments/1.json
  def update
    if @instrument.update(instrument_params)
      redirect_to @instrument, notice: 'Instrument was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /instruments/1 or /instruments/1.json
  def destroy
    @instrument.destroy
    redirect_to instruments_url, notice: 'Instrument was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_instrument
    @instrument = Instrument.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def instrument_params
    params.require(:instrument).permit(:brand_id, :model, :description, :product_id, :title, :price, :image)
  end

  def check_instrument_policy
    redirect_to root_path, notice: "The user doesn't have permission to do it." unless InstrumentPolicy.new(current_user).access?
  end
end
